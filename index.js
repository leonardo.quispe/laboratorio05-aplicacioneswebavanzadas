'use strict';
var express = require('express');
const app = express();

app.set('view engine', 'ejs');
app.set('views',__dirname + '/views');
app.use(express.static(__dirname + "/public"));

//Rutas
app.get('/home',(req , res) => {
    res.render("index", {
        nombre: 'Leonardo Quispe',
        titulo: 'Laboratorio 5'
    });
});

app.get('/elements',(req , res) => {
   res.render("elements", {
    nombre: 'Leonardo Quispe',
    titulo: 'Laboratorio 5'
    });
});

app.get('/generic',(req , res) => {
    res.render("generic", {
        nombre: 'Leonardo Quispe',
        titulo: 'Laboratorio 5'
    });
});

//Servidor web: 3000
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`server listening on port ${ PORT }`);
});

module.exports = app;